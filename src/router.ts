import Vue from "vue";
import Router from "vue-router";
import Home from "./views/Home.vue";
import LoginForm from "./components/LoginForm.vue";
import CarConfigurator from "./views/CarConfigurator.vue";
import ViewCarConfig from "./views/carConfiguration/viewCarConfig.vue";
import EditCarConfig from "./views/carConfiguration/editCarConfig.vue";


Vue.use(Router);

export default new Router({
  mode: "history",
  base: process.env.BASE_URL,
  routes: [
    {
      path: "/",
      name: "home",
      components: {
        main: Home
      },
      children: [
        {
          path: "login",
          name: "login",
          components: {
            main: LoginForm
          }
        },
        {
          path: "car-configurator",
          name: "carConfigurator",
          components: {
            main: CarConfigurator
          },
          children: [
            {
              path: 'view/:carConfigId',
              name: 'viewCarConfiguration',
              components: {
                main: ViewCarConfig
              },
            },
            {
              path: 'edit/:carConfigId?',
              name: 'editCarConfiguration',
              components: {
                main: EditCarConfig
              }
            }    
          ]
        }
      ]
    }
  ]
});
