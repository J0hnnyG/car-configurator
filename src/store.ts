import Vue from "vue";
import Vuex from "vuex";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    userLoggedIn: false
  },
  mutations: {
    setUserLogIn(state, payload) {
      state.userLoggedIn = payload;
    }
  },
  actions: {
    setUserLogIn(context, payload) {
      context.commit('setUserLogIn', payload);
    }
  }
});
