interface IListItem {
    id: string;
    name: string;
}

export default class ListItem implements IListItem {
    public id: string = '';
    public name: string = '';

    constructor(fields?: IListItem) {
        if (fields) {
            this.id = fields.id || '';
            this.name = fields.name || '';
        }
    }
}
